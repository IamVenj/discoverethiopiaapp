import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SearchFlight extends StatefulWidget {
  SearchFlight({Key key}) : super(key: key);

  @override
  _SearchFlightState createState() => _SearchFlightState();
}

class _SearchFlightState extends State<SearchFlight>
    with TickerProviderStateMixin {
  TextEditingController arrivalAtController = TextEditingController();
  TextEditingController departedFromController = TextEditingController();
  TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: height * 0.45,
              child: Stack(
                children: <Widget>[
                  Container(
                    height: height * 0.4,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage('images/flight/globe.png'))),
                  ),
                  Container(
                    height: height * 0.4,
                    decoration:
                        BoxDecoration(color: Colors.black.withOpacity(0.82)),
                  ),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: width * 0.1),
                        child: Row(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topCenter,
                              child: IconButton(
                                icon: Icon(
                                  Icons.arrow_back_ios,
                                  color: Colors.white,
                                ),
                                onPressed: () => Navigator.of(context).pop(),
                              ),
                            ),
                            Container(
                              width: width * 0.8,
                              alignment: Alignment.topCenter,
                              child: Text(
                                "Search Flight",
                                textScaleFactor: 0.7,
                                style: TextStyle(
                                    fontSize: width * 0.07,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        width: width,
                        height: height * 0.11,
                        child: SvgPicture.asset("images/flight/plane.svg"),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            width * 0.04, 0, width * 0.04, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              width: width * 0.4,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Departure From",
                                    textScaleFactor: 0.7,
                                    style: TextStyle(
                                        fontSize: width * 0.056,
                                        color: Colors.white70),
                                  ),
                                  departedFromController.text.isNotEmpty
                                      ? Text(
                                          departedFromController.text,
                                          textScaleFactor: 0.7,
                                          style: TextStyle(
                                              fontSize: width * 0.1,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        )
                                      : Container(),
                                  TextFormField(
                                    controller: departedFromController,
                                    decoration: InputDecoration(
                                        contentPadding:
                                            EdgeInsets.all(height * 0.006),
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.white60,
                                                style: BorderStyle.solid)),
                                        hintText:
                                            'Type where you are departing from',
                                        hintStyle:
                                            TextStyle(color: Colors.white60)),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: width * 0.05,
                                        fontWeight: FontWeight.bold),
                                    keyboardType: TextInputType.emailAddress,
                                    validator: (val) => val.length == 0
                                        ? 'Your destination is required!'
                                        : null,
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: width * 0.4,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Arrival at",
                                    textScaleFactor: 0.7,
                                    style: TextStyle(
                                        fontSize: width * 0.056,
                                        color: Colors.white70),
                                  ),
                                  arrivalAtController.text.isNotEmpty
                                      ? Text(
                                          arrivalAtController.text,
                                          textScaleFactor: 0.7,
                                          style: TextStyle(
                                              fontSize: width * 0.1,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        )
                                      : Container(),
                                  TextFormField(
                                    controller: arrivalAtController,
                                    decoration: InputDecoration(
                                        contentPadding:
                                            EdgeInsets.all(height * 0.006),
                                        hintText: 'Type your Destination',
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.white60,
                                                style: BorderStyle.solid)),
                                        hintStyle:
                                            TextStyle(color: Colors.white60)),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: width * 0.05,
                                        fontWeight: FontWeight.bold),
                                    keyboardType: TextInputType.emailAddress,
                                    validator: (val) => val.length == 0
                                        ? 'Your destination is required!'
                                        : null,
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: height * 0.02,
                      ),
                      Container(
                        width: width,
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle, color: Colors.white),
                            ),
                            IconButton(
                              splashColor: Colors.white12,
                              tooltip: "Exchange Departure and Arrival",
                              icon: Icon(
                                Icons.compare_arrows,
                                color: Colors.white,
                                size: width * 0.08,
                              ),
                              onPressed: () {},
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle, color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: height * 0.02,
                      ),
                      Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(width * 0.08)),
                        elevation: 20,
                        margin: EdgeInsets.only(
                            left: width * 0.05, right: width * 0.05),
                        child: TabBar(
                          controller: _tabController,
                          indicator: BoxDecoration(
                              color: Colors.black,
                              borderRadius:
                                  BorderRadius.circular(width * 0.08)),
                          indicatorColor: Colors.transparent,
                          unselectedLabelColor: Colors.grey,
                          labelColor: Colors.white,
                          tabs: <Widget>[
                            Container(
                              height: height * 0.06,
                              alignment: Alignment.center,
                              child: Text(
                                "One Way",
                                textScaleFactor: 0.7,
                                style: TextStyle(
                                  fontSize: width * 0.05,
                                ),
                              ),
                            ),
                            Container(
                              height: height * 0.06,
                              alignment: Alignment.center,
                              child: Text(
                                "Round Trip",
                                textScaleFactor: 0.7,
                                style: TextStyle(
                                  fontSize: width * 0.05,
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Container(
                height: height * 0.55,
                child: TabBarView(
                  controller: _tabController,
                  children: <Widget>[tabBarView1(), tabBarView2()],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget tabBarView1() {
    return Container();
  }

  Widget tabBarView2() {
    return Container();
  }
}
