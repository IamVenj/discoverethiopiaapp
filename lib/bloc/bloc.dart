import 'dart:async';

abstract class BlocBase {
  void dispose();
}

class CarouselActive implements BlocBase {
  StreamController<int> carouselActiveController =
      StreamController<int>.broadcast();

  @override
  void dispose() {
    carouselActiveController.close();
  }
}
