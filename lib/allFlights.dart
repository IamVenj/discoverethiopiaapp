import 'package:flutter/material.dart';

class AllScreen extends StatefulWidget {
  AllScreen({Key key}) : super(key: key);

  @override
  _AllScreenState createState() => _AllScreenState();
}

class SomeData {
  final String image;
  final String name;
  final String slug;
  final List<Tags> tags;
  final String price;

  SomeData(this.image, this.name, this.slug, this.tags, this.price);
}

class Tags {
  final String tags;
  Tags(this.tags);
}

class _AllScreenState extends State<AllScreen> {
  List<SomeData> someData = [
    SomeData(
        'images/experience/2.jpg',
        'Santorin',
        'Greece is a beautiful country with a coastline of about 1...',
        [Tags('Preferential'), Tags('Population'), Tags('Country')],
        '60'),
    SomeData(
        'images/experience/3.jpg',
        'Santorin',
        'Greece is a beautiful country with a coastline of about 1...',
        [Tags('Preferential'), Tags('Population'), Tags('Country')],
        '60'),
    SomeData(
        'images/experience/4.jpg',
        'Santorin',
        'Greece is a beautiful country with a coastline of about 1...',
        [Tags('Preferential'), Tags('Population'), Tags('Country')],
        '60'),
    SomeData(
        'images/experience/2.jpg',
        'Santorin',
        'Greece is a beautiful country with a coastline of about 1...',
        [Tags('Preferential'), Tags('Population'), Tags('Country')],
        '60'),
    SomeData(
        'images/hotel/3.jpg',
        'Santorin',
        'Greece is a beautiful country with a coastline of about 1...',
        [
          Tags('Preferential'),
          Tags('Population'),
          Tags('Country'),
          Tags('Country'),
          Tags('Country')
        ],
        '60'),
    SomeData(
        'images/hotel/5.jpg',
        'Santorin',
        'Greece is a beautiful country with a coastline of about 1...',
        [Tags('Preferential'), Tags('Population'), Tags('Country')],
        '60'),
    SomeData(
        'images/hotel/3.jpg',
        'Santorin',
        'Greece is a beautiful country with a coastline of about 1...',
        [Tags('Preferential'), Tags('Population'), Tags('Country')],
        '60'),
  ];

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
        body: CustomScrollView(
      slivers: <Widget>[
        new SliverAppBar(
          expandedHeight: height * 0.25,
          // floating: true,
          pinned: true,
          actions: <Widget>[
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.filter_list, size: width * 0.06,),
            )
          ],
          leading: IconButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: Icon(
                Icons.arrow_back_ios,
              )),
          flexibleSpace: new FlexibleSpaceBar(
            title: Text(
              'Flights'.toUpperCase(),
              textScaleFactor: 0.7,
              style: TextStyle(
                  letterSpacing: width * 0.003,
                  fontSize: width * 0.07,
                  fontWeight: FontWeight.bold,
                  color: Colors.black54
                  ),
            ),
            background: Stack(children: <Widget>[
              Positioned.fill(
                child: Image.asset(
                  'images/hotel/3.jpg',
                  fit: BoxFit.cover,
                ),
              ),
              Positioned.fill(
                child: Container(
                  height: height * 0.25,
                  width: width,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(colors: [
                    Colors.white38.withOpacity(0),
                    Colors.white.withOpacity(1)
                  ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
                ),
              )
            ]),
          ),
        ),
        new AllCarryingCard(
          someData: someData,
        )
      ],
    ));
  }
}

class AllCarryingCard extends StatelessWidget {
  final List<SomeData> someData;
  const AllCarryingCard({Key key, @required this.someData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return SliverList(
      delegate: new SliverChildBuilderDelegate((context, i) {
        return Padding(
          padding: EdgeInsets.only(top: height * 0.01),
          child: Container(
            height: height * 0.2,
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(width * 0.02)),
              elevation: 5,
              margin: EdgeInsets.only(
                  left: width * 0.02,
                  right: width * 0.02,
                  bottom: width * 0.02),
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                        left: width * 0.035,
                        top: width * 0.035,
                        bottom: width * 0.035),
                    child: Container(
                      width: width * 0.385,
                      height: height * 0.2,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(width * 0.02),
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage(someData[i].image))),
                    ),
                  ),
                  Container(
                    width: width * 0.54,
                    padding: EdgeInsets.only(
                        left: width * 0.035,
                        right: width * 0.035,
                        top: width * 0.035),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          someData[i].name,
                          textScaleFactor: 0.7,
                          style: TextStyle(
                              fontSize: width * 0.06,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: height * 0.01,
                        ),
                        Text(
                          someData[i].slug,
                          textScaleFactor: 0.7,
                          style: TextStyle(
                              fontSize: width * 0.045, color: Colors.grey),
                        ),
                        someData[i].tags.length > 3
                            ? Flexible(
                                child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    mainAxisSize: MainAxisSize.max,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        mainAxisSize: MainAxisSize.max,
                                        children: someData[i]
                                            .tags
                                            .getRange(0, 2)
                                            .map((tag) {
                                          return Container(
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        width * 0.01),
                                                color: Colors.grey
                                                    .withOpacity(0.2)),
                                            padding:
                                                EdgeInsets.all(width * 0.01),
                                            child: Text(
                                              tag.tags,
                                              textScaleFactor: 0.7,
                                              style: TextStyle(
                                                  color: Color(0xFF000000)),
                                            ),
                                          );
                                        }).toList(),
                                      ),
                                      Icon(Icons.chevron_right)
                                    ]),
                              )
                            : Flexible(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: someData[i].tags.map((tag) {
                                    return Container(
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(
                                              width * 0.01),
                                          color: Colors.grey.withOpacity(0.2)),
                                      padding: EdgeInsets.all(width * 0.01),
                                      child: Text(
                                        tag.tags,
                                        textScaleFactor: 0.7,
                                        style:
                                            TextStyle(color: Color(0xFF000000)),
                                      ),
                                    );
                                  }).toList(),
                                ),
                              ),
                        SizedBox(
                          height: height * 0.01,
                        ),
                        Text(
                          "\$${someData[i].price}",
                          textScaleFactor: 0.7,
                          style: TextStyle(
                              fontSize: width * 0.08,
                              fontWeight: FontWeight.w700,
                              color: Color(0xFF000000)),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      }, childCount: someData.length),
    );
  }
}
