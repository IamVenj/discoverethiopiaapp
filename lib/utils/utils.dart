import 'package:shared_preferences/shared_preferences.dart';

class Utils {

}

class SharedPref {
  SharedPreferences _sharedPreferences;

  Future<SharedPreferences> _sharedPref() async {
    _sharedPreferences = await SharedPreferences.getInstance();
    return _sharedPreferences;
  }

  getPreference(key) {
    _sharedPref().then((preference){
      return preference.getString(key);
    });
  }

  setPreference(String key, String value) {
    _sharedPref().then((preference) {
      return preference.setString(key, value);
    });
  }

  Future<bool> contains(String key) async {
    List<bool> preferenceArray = [];
    await _sharedPref().then((preference) {
      preferenceArray.add(preference.containsKey(key));
    });
    return preferenceArray[0];
  }

}