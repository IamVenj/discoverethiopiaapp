import 'dart:math';

import 'package:discover_ethiopia/flight.dart';
import 'package:discover_ethiopia/tour_detail.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class Experience {
  final String image;
  final bool bookmark;
  final String title;
  final String location;

  Experience(this.image, this.bookmark, this.title, this.location);
}

class Tour {
  final String image;
  final String tourName;
  final String slug;
  final int rating;
  final int commentCount;

  Tour(this.image, this.tourName, this.slug, this.rating, this.commentCount);
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  List<Experience> experience = [
    Experience(
        'images/experience/3.jpg', true, 'Kayaking at the village', 'Norway'),
    Experience('images/experience/4.jpg', false,
        'Breathtaking Antelope Canyon Tour', 'Arizona, USA'),
    Experience(
        'images/experience/4.jpg', false, 'Kayaking at the village', 'Norway'),
    Experience('images/experience/3.jpg', true,
        'Breathtaking Antelope Canyon Tour', 'Arizona, USA'),
    Experience(
        'images/experience/2.jpg', true, 'Kayaking at the village', 'Norway'),
  ];

  List<Experience> hotels = [
    Experience('images/hotel/3.jpg', true, 'Montserrat Hotel',
        'Ethiopia, Addis Ababa'),
    Experience(
        'images/hotel/5.jpg', true, 'Saromaria Hotel', 'Ethiopia, Addis Ababa'),
    Experience('images/hotel/3.jpg', true, 'Molalegn Hotel', 'Ethiopia, Jimma'),
    Experience(
        'images/hotel/3.jpg', true, 'Kuralew Hotel', 'Ethiopia, Hawassa'),
    Experience(
        'images/hotel/5.jpg', true, 'Beg ketema Hotel', 'Ethiopia, Wollega'),
    Experience('images/hotel/3.jpg', true, 'Etege Taitu Hotel',
        'Ethiopia, Addis Ababa'),
    Experience(
        'images/hotel/5.jpg', true, 'Jupiter Hotel', 'Ethiopia, Addis Ababa'),
  ];

  List<Tour> tours = [
    Tour(
        'images/experience/4.jpg',
        'tourName',
        'The republic of Maldives is anarchioelago country in the indian ocean. it is about 600 kilometeres...',
        3,
        422),
    Tour(
        'images/experience/3.jpg',
        'tourName',
        'The republic of Maldives is anarchioelago country in the indian ocean. it is about 600 kilometeres...',
        3,
        422),
    Tour(
        'images/experience/2.jpg',
        'tourName',
        'The republic of Maldives is anarchioelago country in the indian ocean. it is about 600 kilometeres...',
        3,
        422),
    Tour(
        'images/experience/4.jpg',
        'tourName',
        'The republic of Maldives is anarchioelago country in the indian ocean. it is about 600 kilometeres...',
        3,
        422),
  ];

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
    int currentIndex = 0;

    return Scaffold(
      key: _scaffoldKey,
      drawer: Drawer(),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentIndex,
        onTap: (int selectedIndex) {
          setState(() {
            currentIndex = selectedIndex;
          });
        },
        unselectedFontSize: 0,
        selectedFontSize: 0,
        backgroundColor: Colors.white,
        items: [
          BottomNavigationBarItem(
              icon: Icon(
                Icons.nature,
                color: Colors.black54,
              ),
              title: Text("")),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.airplay,
                color: Colors.black54,
              ),
              title: Text("")),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.add_alarm,
                color: Colors.black54,
              ),
              title: Text("")),
        ],
      ),
      body: Stack(
        children: <Widget>[
          Container(
            height: height * 0.48,
            width: width,
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('images/experience/4.jpg'))),
          ),
          Container(
            height: height * 0.48,
            width: width,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.white.withOpacity(0.8), Color(0xFFFAFAFA)],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter)),
          ),
          Container(
            height: height,
            width: width * 0.2,
            decoration:
                BoxDecoration(color: Color(0x70000000).withOpacity(0.05)),
          ),
          Container(
            height: height,
            width: width,
            padding: EdgeInsets.only(left: width * 0.03, top: height * 0.045),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    GestureDetector(
                        onTap: () {
                          _scaffoldKey.currentState.openDrawer();
                        },
                        child: Container(
                            width: width * 0.1,
                            height: height * 0.025,
                            child: Image.asset('images/menu.png'))),
                    Padding(
                      padding: EdgeInsets.only(right: width * 0.06),
                      child: Container(
                        height: height * 0.05,
                        width: width * 0.11,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [
                              BoxShadow(
                                  blurRadius: 3,
                                  spreadRadius: 1,
                                  color: Colors.grey.withOpacity(0.4))
                            ],
                            image: DecorationImage(
                                image: AssetImage('images/experience/3.jpg'),
                                fit: BoxFit.cover,
                                alignment: Alignment.center)),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: height * 0.03,
                ),
                Text(
                  'Hello JD, welcome to Discover Ethiopia',
                  textScaleFactor: 0.7,
                  style: TextStyle(
                      fontSize: width * 0.1, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: height * 0.02,
                ),
                CategeoryCard(),
                SizedBox(
                  height: height * 0.01,
                ),
                Center(
                  child: HorizontalTabLayout(
                    experience: experience,
                    hotel: hotels,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class CategeoryCard extends StatelessWidget {
  const CategeoryCard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
      height: height * 0.12,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Column(
            children: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => FlightScreen()));
                },
                child: Container(
                  padding: EdgeInsets.all(width * 0.03),
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 10,
                          color: Colors.black12.withOpacity(0.01),
                          spreadRadius: 0,
                        )
                      ],
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(width * 0.02)),
                  child: Icon(
                    Icons.airplanemode_active,
                    color: Colors.black,
                  ),
                ),
              ),
              SizedBox(
                height: height * 0.01,
              ),
              Text(
                'Flight',
                textScaleFactor: 0.7,
                style: TextStyle(
                    fontSize: width * 0.05, fontWeight: FontWeight.bold),
              )
            ],
          ),
          Column(
            children: <Widget>[
              InkWell(
                onTap: () {},
                child: Container(
                  padding: EdgeInsets.all(width * 0.03),
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 10,
                          color: Colors.black12.withOpacity(0.01),
                          spreadRadius: 0,
                        )
                      ],
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(width * 0.02)),
                  child: Icon(
                    Icons.local_hotel,
                    color: Colors.black,
                  ),
                ),
              ),
              SizedBox(
                height: height * 0.01,
              ),
              Text(
                'Hotel',
                textScaleFactor: 0.7,
                style: TextStyle(
                    fontSize: width * 0.05, fontWeight: FontWeight.bold),
              )
            ],
          ),
          Column(
            children: <Widget>[
              InkWell(
                onTap: () {},
                child: Container(
                  padding: EdgeInsets.all(width * 0.03),
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 10,
                          color: Colors.black12.withOpacity(0.01),
                          spreadRadius: 0,
                        )
                      ],
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(width * 0.02)),
                  child: Icon(
                    Icons.local_taxi,
                    color: Colors.black,
                  ),
                ),
              ),
              SizedBox(
                height: height * 0.01,
              ),
              Text(
                'Car',
                textScaleFactor: 0.7,
                style: TextStyle(
                    fontSize: width * 0.05, fontWeight: FontWeight.bold),
              )
            ],
          ),
          Column(
            children: <Widget>[
              InkWell(
                onTap: () {},
                child: Container(
                  padding: EdgeInsets.all(width * 0.03),
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 10,
                          color: Colors.black12.withOpacity(0.01),
                          spreadRadius: 0,
                        )
                      ],
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(width * 0.02)),
                  child: Icon(
                    Icons.directions_boat,
                    color: Colors.black,
                  ),
                ),
              ),
              SizedBox(
                height: height * 0.01,
              ),
              Text(
                'Tour',
                textScaleFactor: 0.7,
                style: TextStyle(
                    fontSize: width * 0.05, fontWeight: FontWeight.bold),
              )
            ],
          ),
          Column(
            children: <Widget>[
              InkWell(
                onTap: () {},
                child: Container(
                  padding: EdgeInsets.all(width * 0.03),
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 10,
                          color: Colors.black12.withOpacity(0.01),
                          spreadRadius: 0,
                        )
                      ],
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(width * 0.02)),
                  child: Icon(
                    Icons.home,
                    color: Colors.black,
                  ),
                ),
              ),
              SizedBox(
                height: height * 0.01,
              ),
              Text(
                'Housing',
                textScaleFactor: 0.7,
                style: TextStyle(
                    fontSize: width * 0.05, fontWeight: FontWeight.bold),
              )
            ],
          ),
        ],
      ),
    );
  }
}

class HorizontalTabLayout extends StatefulWidget {
  final List<Experience> experience;
  final List<Experience> hotel;
  HorizontalTabLayout(
      {Key key, @required this.experience, @required this.hotel})
      : super(key: key);

  @override
  _HorizontalTabLayoutState createState() => _HorizontalTabLayoutState();
}

class _HorizontalTabLayoutState extends State<HorizontalTabLayout> {
  int selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
        height: height * 0.55,
        width: width,
        child: Stack(
          children: <Widget>[
            Positioned(
              left: -width * 0.06,
              bottom: 0,
              top: 0,
              width: width * 0.3,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: width * 0.2),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      TabText(
                        isSelected: selectedIndex == 0,
                        text: "Best Offers",
                        onTabTap: () {
                          onTabTap(0);
                        },
                      ),
                      TabText(
                        isSelected: selectedIndex == 1,
                        text: "Adventures",
                        onTabTap: () {
                          onTabTap(1);
                        },
                      ),
                      TabText(
                        isSelected: selectedIndex == 2,
                        text: "Featured News",
                        onTabTap: () {
                          onTabTap(2);
                        },
                      ),
                    ]),
              ),
            ),
            Positioned.fill(
              left: width * 0.17,
              child: Container(width: width * 0.8, child: setTabDataByType()),
            )
          ],
        ));
  }

  onTabTap(int i) {
    setState(() {
      selectedIndex = i;
    });
  }

  Widget setTabDataByType() {
    if (getTypeByIndex(selectedIndex) == "Best Offers") {
      return HomeData(
        experience: widget.experience,
      );
    } else if (getTypeByIndex(selectedIndex) == "Adventures") {
      return HomeData(
        experience: widget.hotel,
      );
    } else if (getTypeByIndex(selectedIndex) == "Featured News") {
      return HomeData(
        experience: widget.experience,
      );
    } else {
      return HomeData(
        experience: widget.experience,
      );
    }
  }

  String getTypeByIndex(int index) {
    switch (index) {
      case 0:
        return "Best Offers";
      case 1:
        return "Adventures";
      case 2:
        return "Featured News";
      default:
        return "Best Offers";
    }
  }
}

class TabText extends StatelessWidget {
  final bool isSelected;
  final String text;
  final Function onTabTap;

  const TabText({Key key, this.isSelected = false, this.text, this.onTabTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Transform.rotate(
      angle: -1.58,
      child: GestureDetector(
        onTap: onTabTap,
        child: Column(
          children: <Widget>[
            Text(text,
                textScaleFactor: 0.7,
                style: isSelected
                    ? TextStyle(
                        fontSize: width * 0.06, fontWeight: FontWeight.bold)
                    : TextStyle(
                        fontSize: width * 0.06,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey)),
            SizedBox(
              height: height * 0.01,
            ),
            isSelected
                ? Container(
                    height: height * 0.01,
                    width: width * 0.02,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.black),
                  )
                : Container(
                    height: height * 0.01,
                  )
          ],
        ),
      ),
    );
  }
}

class HomeData extends StatefulWidget {
  final List<Experience> experience;
  HomeData({Key key, @required this.experience}) : super(key: key);

  @override
  _HomeDataState createState() => _HomeDataState();
}

class _HomeDataState extends State<HomeData> {
  PageController _pageController;
  double pageOffset = 0;
  double viewPortFraction = 0.8;

  @override
  void initState() {
    _pageController =
        PageController(initialPage: 0, viewportFraction: viewPortFraction)
          ..addListener(() {
            setState(() {
              pageOffset = _pageController.page;
            });
          });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return PageView.builder(
      itemCount: widget.experience.length,
      scrollDirection: Axis.horizontal,
      controller: _pageController,
      itemBuilder: (context, index) {
        double scale = max(viewPortFraction,
            1 - ((pageOffset - index).abs()) + viewPortFraction);

        double angle = (pageOffset - index).abs();
        if (angle > 0.5) {
          angle = 1 - angle;
        }

        return GestureDetector(
          onTap: () {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => TourDetail()));
          },
          child: Container(
            height: height * 0.55,
            width: width * 0.8,
            padding: EdgeInsets.only(
                top: 50 - scale * 20,
                left: width * 0.02,
                right: width * 0.02,
                bottom: 50 - scale * 20),
            child: Transform(
              transform: Matrix4.identity()
                ..setEntry(3, 2, 0.002)
                ..rotateY(angle),
              alignment: Alignment.center,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(width * 0.05),
                ),
                elevation: width * 0.015,
                margin: EdgeInsets.all(0),
                child: Stack(
                  children: <Widget>[
                    Positioned.fill(
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(width * 0.05),
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage(
                                  widget.experience[index].image,
                                ))),
                      ),
                    ),
                    Positioned.fill(
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(width * 0.05),
                            gradient: LinearGradient(
                                colors: [
                                  Colors.black.withOpacity(0.2),
                                  Colors.black87
                                ],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter)),
                      ),
                    ),
                    Positioned(
                      bottom: height * 0.02,
                      left: width * 0.04,
                      child: Container(
                        width: width * 0.5,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "${widget.experience[index].title}",
                              textScaleFactor: 0.7,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: width * 0.06,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: height * 0.01,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Icon(
                                  Icons.location_on,
                                  color: Colors.white,
                                  size: width * 0.04,
                                ),
                                Text(
                                  "${widget.experience[index].location}",
                                  textScaleFactor: 0.7,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: width * 0.05),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    widget.experience[index].bookmark
                        ? Positioned(
                            top: width * 0.01,
                            right: width * 0.02,
                            child: IconButton(
                                onPressed: () {},
                                icon: Icon(
                                  Icons.favorite,
                                  color: Colors.white,
                                )),
                          )
                        : Positioned(
                            top: width * 0.01,
                            right: width * 0.02,
                            child: IconButton(
                                onPressed: () {},
                                icon: Icon(
                                  Icons.favorite_border,
                                  color: Colors.white,
                                )),
                          ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
