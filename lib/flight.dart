import 'package:discover_ethiopia/allFlights.dart';
import 'package:discover_ethiopia/bloc/bloc.dart';
import 'package:discover_ethiopia/flight_detail.dart';
import 'package:discover_ethiopia/searchFlight.dart';
import 'package:flutter/material.dart';

class FlightScreen extends StatefulWidget {
  FlightScreen({Key key}) : super(key: key);

  @override
  _FlightScreenState createState() => _FlightScreenState();
}

class Flight {
  final String image;
  final String usersLocation;
  final String from;
  final String to;
  final String departureDate;
  final String departureTime;
  final String arrivalDate;
  final String arrivalTime;
  final String tripType;

  Flight(this.image, this.usersLocation, this.from, this.to, this.departureDate,
      this.arrivalDate, this.arrivalTime, this.departureTime, this.tripType);
}

class _FlightScreenState extends State<FlightScreen> {
  CarouselActive _carouselActive;

  @override
  void initState() {
    super.initState();
    _carouselActive = new CarouselActive();
  }

  List<Flight> flights = [
    Flight(
        'images/hotel/3.jpg',
        "Paris",
        "Paris, Edenberg",
        "Ethiopia, Addis Ababa",
        "March 19",
        "March 20",
        "02:30",
        "04:10",
        "roundtrip"),
    Flight(
        'images/hotel/3.jpg',
        "Paris",
        "Paris, Edenberg",
        "Ethiopia, Addis Ababa",
        "March 19",
        "March 20",
        "02:30",
        "04:10",
        "oneway"),
    Flight('images/experience/4.jpg', "Paris", "Paris, Edenberg",
        "ETH, Addis Ababa", "March 19", "March 20", "02:30", "04:10", "oneway"),
    Flight('images/hotel/5.jpg', "Paris", "Paris, Edenberg", "ETH, Addis Ababa",
        "March 19", "March 20", "02:30", "04:10", "roundtrip"),
    Flight('images/hotel/5.jpg', "Paris", "Paris, Edenberg", "ETH, Addis Ababa",
        "March 19", "March 20", "02:30", "04:10", "oneway"),
  ];

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: height,
            width: width * 0.2,
            decoration:
                BoxDecoration(color: Color(0x70000000).withOpacity(0.05)),
          ),
          Container(
              height: height,
              width: width,
              padding: EdgeInsets.only(left: width * 0.01, top: height * 0.033),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.arrow_back_ios),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            right: width * 0.06,
                          ),
                          child: Container(
                            height: height * 0.05,
                            width: width * 0.11,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                boxShadow: [
                                  BoxShadow(
                                      blurRadius: 3,
                                      spreadRadius: 1,
                                      color: Colors.grey.withOpacity(0.4))
                                ],
                                image: DecorationImage(
                                    image:
                                        AssetImage('images/experience/3.jpg'),
                                    fit: BoxFit.cover,
                                    alignment: Alignment.center)),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: height * 0.03,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: width * 0.05),
                      child: Row(
                        children: <Widget>[
                          Container(
                            width: width * 0.6,
                            child: Text(
                              "Let's pack for your trip",
                              textScaleFactor: 0.7,
                              style: TextStyle(
                                  fontSize: width * 0.12,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            width: width * 0.3,
                            child: OutlineButton(
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => AllScreen()));
                              },
                              highlightedBorderColor: Color(0xFF000000),
                              child: Text("Show All"),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: height * 0.02,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: width * 0.05),
                      child: Container(
                        width: width * 0.7,
                        child: Text(
                          "Use one of our suggestions or search for your flight",
                          textScaleFactor: 0.7,
                          style: TextStyle(fontSize: width * 0.07),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: height * 0.02,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: width * 0.05),
                      child: Container(
                        height: height * 0.45,
                        child: FlightCard(
                          flights: flights,
                          carouselActive: _carouselActive,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: height * 0.08,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: width * 0.05),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                              child: flightCountIndex(),
                              height: height * 0.01,
                              width: width * 0.7),
                          Padding(
                            padding: EdgeInsets.only(right: width * 0.04),
                            child: FloatingActionButton(
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => SearchFlight()));
                              },
                              heroTag: null,
                              backgroundColor: Color(0xFF000000),
                              child: Icon(Icons.search),
                            ),
                          ),
                        ],
                      ),
                    )
                  ]))
        ],
      ),
    );
  }

  Widget flightCountIndex() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      height: height * 0.01,
      width: width * 0.7,
      child: StreamBuilder<int>(
          stream: _carouselActive.carouselActiveController.stream,
          initialData: 0,
          builder: (context, AsyncSnapshot<int> snapshot) {
            return ListView.builder(
              itemCount: flights.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, int i) {
                return Padding(
                  padding: EdgeInsets.only(right: width * 0.022),
                  child: (snapshot.data == i)
                      ? Container(
                          width: width * 0.02,
                          height: height * 0.01,
                          decoration: BoxDecoration(
                              color: Color(0xFF000000),
                              borderRadius: BorderRadius.circular(width)),
                        )
                      : Container(
                          width: width * 0.02,
                          height: height * 0.01,
                          decoration: BoxDecoration(
                              color: Colors.grey,
                              borderRadius: BorderRadius.circular(width)),
                        ),
                );
              },
            );
          }),
    );
  }
}

class FlightCard extends StatefulWidget {
  final List<Flight> flights;
  final CarouselActive carouselActive;
  FlightCard({Key key, @required this.flights, @required this.carouselActive})
      : super(key: key);

  @override
  _FlightCardState createState() => _FlightCardState(flights, carouselActive);
}

class _FlightCardState extends State<FlightCard> {
  List<Flight> flights;
  CarouselActive carouselActive;

  _FlightCardState(this.flights, this.carouselActive);
  PageController _pageController;
  double pageOffset = 0;
  double viewPortFraction = 0.8;

  @override
  void initState() {
    _pageController =
        PageController(initialPage: 0, viewportFraction: viewPortFraction)
          ..addListener(() {
            setState(() {
              pageOffset = _pageController.page;
            });
            activateCard();
          });
    super.initState();
  }

  activateCard() {
    carouselActive.carouselActiveController.sink
        .add(_pageController.page.round());
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => FlightDetailScreen()));
      },
      child: PageView.builder(
        controller: _pageController,
        itemCount: flights.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int i) {
          return Container(
            height: height * 0.45,
            width: width * 0.1,
            padding: EdgeInsets.only(right: width * 0.01),
            child: Stack(children: <Widget>[
              Positioned.fill(
                child: Image.asset(
                  flights[i].image,
                  fit: BoxFit.cover,
                  width: width,
                  height: height,
                  // alignment: Alignment(-pageOffset.abs(), 0),
                ),
              ),
              Positioned.fill(
                child: DecoratedBox(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [Colors.black.withOpacity(0.3), Colors.black],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter)),
                ),
              ),
              Positioned(
                bottom: width * 0.1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: width * 0.04),
                      child: Text(
                        '${flights[i].usersLocation}',
                        textScaleFactor: 0.7,
                        style: TextStyle(
                            fontSize: width * 0.08,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    ),
                    SizedBox(
                      height: height * 0.01,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: width * 0.04),
                      child: Row(
                        children: <Widget>[
                          Text(
                            '${flights[i].from}',
                            textScaleFactor: 0.7,
                            style: TextStyle(
                                fontSize: width * 0.04,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          SizedBox(
                            width: width * 0.01,
                          ),
                          Text(
                            '-->',
                            textScaleFactor: 0.7,
                            style: TextStyle(
                                color: Colors.grey, fontSize: width * 0.06),
                          ),
                          // Icon(Icons.arrow_forward_ios, color: Colors.grey, size: width * 0.04,),
                          SizedBox(
                            width: width * 0.01,
                          ),
                          Text(
                            '${flights[i].to}',
                            textScaleFactor: 0.7,
                            style: TextStyle(
                                fontSize: width * 0.04,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: height * 0.02,
                    ),
                    (flights[i].tripType == "roundtrip")
                        ? Row(
                            children: <Widget>[],
                          )
                        : Container(),
                    (flights[i].tripType == "oneway")
                        ? Row(
                            children: <Widget>[],
                          )
                        : Container(),
                    SizedBox(
                      height: height * 0.03,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: width * 0.04),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.grey,
                            size: width * 0.03,
                          ),
                          Text(
                            '${flights[i].departureDate} | ${flights[i].departureTime}',
                            textScaleFactor: 0.7,
                            style: TextStyle(
                                fontSize: width * 0.05,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: height * 0.01,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: width * 0.04),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.grey,
                            size: width * 0.03,
                          ),
                          Text(
                            '${flights[i].arrivalDate} | ${flights[i].arrivalTime}',
                            textScaleFactor: 0.7,
                            style: TextStyle(
                                fontSize: width * 0.05,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              )
            ]),
          );
        },
      ),
    );
  }
}
