import 'package:discover_ethiopia/splashscreen.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Discover Ethiopia',
      theme: ThemeData(
        primaryColor: Colors.white,
        primaryColorDark: Colors.white30,
        accentColor: Colors.blue,
        fontFamily: 'ProductSans',
      ),
      debugShowCheckedModeBanner: false,
      home: new SplashScreen(),
    );
  }
}