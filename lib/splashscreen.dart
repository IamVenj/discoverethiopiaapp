import 'package:discover_ethiopia/getstarted.dart';
import 'package:discover_ethiopia/homescreen.dart';
import 'package:discover_ethiopia/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

startTime(BuildContext context) async {
  bool getStartedState = await SharedPref().contains('getStarted');
  getStartedState
      ? Future.delayed(Duration(seconds: 5), () => navigateToHomePage(context))
      : Future.delayed(
          Duration(seconds: 5), () => navigateToGetStarted(context));
}

navigateToGetStarted(BuildContext context) {
  Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) {
    return GetStarted();
  }));
}

navigateToHomePage(BuildContext context) {
  Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) {
    return HomeScreen();
  }));
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    startTime(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            height: height * 0.4,
            margin: EdgeInsets.only(left: width * 0.02, right: width * 0.02),
            child: Image.asset(
              'images/logo.png',
              alignment: Alignment.bottomCenter,
            ),
          ),
          Container(
            height: height * 0.6,
            child: SvgPicture.asset(
              'images/de-splash.svg',
              alignment: Alignment.bottomCenter,
              fit: BoxFit.cover,
            ),
          ),
        ],
      ),
    );
  }
}
