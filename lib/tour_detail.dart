import 'package:flutter/material.dart';

class TourDetailData {
  final String title;
  final String rating;
  final String totalPplWhoRated;
  final String description;
  final List<TourNews> tourNews;
  final List<TourPrice> tourPrice;
  final String totalLikes;
  final String totalComments;
  final String totalShares;
  final bool favorite;
  final String totalPrice;

  TourDetailData(
      this.title,
      this.rating,
      this.totalPplWhoRated,
      this.description,
      this.tourNews,
      this.tourPrice,
      this.totalLikes,
      this.totalComments,
      this.totalShares,
      this.favorite,
      this.totalPrice);
}

class TourNews {
  final String newsTitle;
  final String newsDescription;

  TourNews(this.newsTitle, this.newsDescription);
}

class TourPrice {
  final String priceName;
  final String priceStart;
  final String priceEnd;
  final String standalonePrice;

  TourPrice(
      this.priceName, this.priceStart, this.priceEnd, this.standalonePrice);
}

class TourDetail extends StatefulWidget {
  TourDetail({Key key}) : super(key: key);

  @override
  _TourDetailState createState() => _TourDetailState();
}

class _TourDetailState extends State<TourDetail> {
  TourDetailData tourSampleData = TourDetailData(
      "Kayaking at the village",
      "4",
      "234",
      "Located Just 10 Miles west of Aspen or 16 miles from snowmass up Maroon Creek Road Off Hwy. 82 in a glacial valley. the 14,000-foot peaks (fourteeners) truly epitomize the beauty of the rocky mountains. Hiking trails provide access to plenty pf-photo worthy scnes.",
      [
        TourNews("newsTitle",
            "6 miles from snowmass up Maroon Creek Road Off Hwy. 82 in a glacial valley. the 14,000-foot peaks (fourteeners) truly epitomize the beauty of the rocky mountains. Hiking trails provide access to plenty pf-photo worthy scnes."),
        TourNews("newsTitle",
            "6 miles from snowmass up Maroon Creek Road Off Hwy. 82 in a glacial valley. the 14,000-foot peaks (fourteeners) truly epitomize the beauty of the rocky mountains. Hiking trails provide access to plenty pf-photo worthy scnes."),
        TourNews("newsTitle",
            "6 miles from snowmass up Maroon Creek Road Off Hwy. 82 in a glacial valley. the 14,000-foot peaks (fourteeners) truly epitomize the beauty of the rocky mountains. Hiking trails provide access to plenty pf-photo worthy scnes."),
      ],
      [
        TourPrice("stand", "100", "300", null),
        TourPrice("name", null, null, "400"),
        TourPrice("name", null, null, "400"),
        TourPrice("green", "300", "600", null),
        TourPrice("green", "300", "600", null),
      ],
      "240",
      "121",
      "95",
      true,
      "34500");

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Positioned.fill(
          child: Image.asset(
            "images/experience/4.jpg",
            fit: BoxFit.cover,
            alignment: Alignment.center,
          ),
        ),
        Positioned.fill(
          child: DecoratedBox(
            decoration: BoxDecoration(
                gradient: LinearGradient(
              colors: [Colors.black.withOpacity(0.6), Colors.black],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            )),
          ),
        ),
        Positioned(
          top: height * 0.04,
          child: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        Positioned.fill(
          top: height * 0.27,
          child: Container(
            width: width,
            padding: EdgeInsets.all(width * 0.08),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(width * 0.04),
                color: Colors.white.withOpacity(0.15)),
            margin: EdgeInsets.all(width * 0.04),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: width * 0.6,
                      child: Text(
                        tourSampleData.title,
                        textScaleFactor: 0.7,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: width * 0.1,
                        ),
                      ),
                    ),
                    tourSampleData.favorite
                        ? IconButton(
                            icon: Icon(
                              Icons.favorite,
                              color: Colors.red,
                            ),
                            onPressed: () {},
                          )
                        : IconButton(
                            icon: Icon(
                              Icons.favorite_border,
                              color: Colors.white,
                            ),
                            onPressed: () {},
                          )
                  ],
                ),
                SizedBox(
                  height: height * 0.01,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          "${tourSampleData.totalPplWhoRated} People Score",
                          textScaleFactor: 0.7,
                          style: TextStyle(
                              color: Colors.white, fontSize: width * 0.04),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          "Total Price: ",
                          textScaleFactor: 0.7,
                          style: TextStyle(
                              fontSize: width * 0.04, color: Colors.white),
                        ),
                        Text(
                          "\$${tourSampleData.totalPrice}",
                          textScaleFactor: 0.7,
                          style: TextStyle(
                              fontSize: width * 0.04,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: height * 0.02,
                ),
                Container(
                  height: height * 0.1,
                  child: SingleChildScrollView(
                    child: Text(
                      tourSampleData.description,
                      textScaleFactor: 0.7,
                      style: TextStyle(
                          color: Colors.white, fontSize: width * 0.04),
                    ),
                  ),
                ),
                Divider(
                  color: Colors.white,
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: height * 0.006),
                  child: tourSampleData.tourNews.isNotEmpty
                      ? Text(
                          "Total News - ${tourSampleData.tourNews.length.toString()}",
                          textScaleFactor: 0.7,
                          style: TextStyle(
                              fontSize: width * 0.06,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        )
                      : Container(),
                ),
                tourSampleData.tourNews.isNotEmpty
                    ? Container(
                        height: height * 0.1,
                        child: SingleChildScrollView(
                            child: Column(
                          children: tourSampleData.tourNews.map((news) {
                            return newsWidget(news);
                          }).toList(),
                        )))
                    : Container(),
                SizedBox(
                  height: height * 0.02,
                ),
                tourSampleData.tourPrice.isNotEmpty
                    ? Container(
                        height: height * 0.08,
                        width: width,
                        child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: tourSampleData.tourPrice.map((price) {
                                  return priceWidget(price);
                                }).toList())))
                    : Container(),
                SizedBox(
                  height: height * 0.02,
                ),
                Container(
                  width: width,
                  child: RaisedButton(
                    onPressed: () {},
                    color: Colors.white,
                    child: Text(
                      "Add Review",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SizedBox(
                  height: height * 0.02,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Icon(
                          Icons.favorite,
                          color: Colors.white,
                        ),
                        SizedBox(
                          width: width * 0.01,
                        ),
                        Text(
                          tourSampleData.totalLikes,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: width * 0.05,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Icon(
                          Icons.comment,
                          color: Colors.white,
                        ),
                        SizedBox(
                          width: width * 0.01,
                        ),
                        Text(
                          tourSampleData.totalComments,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: width * 0.05,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Icon(
                          Icons.share,
                          color: Colors.white,
                        ),
                        SizedBox(
                          width: width * 0.01,
                        ),
                        Text(
                          tourSampleData.totalShares,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: width * 0.05,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
        )
      ],
    ));
  }

  Widget newsWidget(TourNews tourNews) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("${tourNews.newsTitle}",
            textScaleFactor: 0.7,
            style: TextStyle(
                fontSize: width * 0.04,
                fontWeight: FontWeight.bold,
                color: Colors.white)),
        SizedBox(
          height: height * 0.01,
        ),
        Text(
          "${tourNews.newsDescription}",
          textScaleFactor: 0.7,
          style: TextStyle(fontSize: width * 0.04, color: Colors.white),
        ),
        SizedBox(
          height: height * 0.01,
        )
      ],
    );
  }

  Widget priceWidget(TourPrice price) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: Colors.white, width: 0.5),
          borderRadius: BorderRadius.circular(width * 0.01)),
      padding: EdgeInsets.all(width * 0.03),
      margin: EdgeInsets.only(right: width * 0.03),
      child: Column(
        children: <Widget>[
          price.standalonePrice == null
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "\$${price.priceStart}",
                      textScaleFactor: 0.7,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                    Text(
                      "-",
                      textScaleFactor: 0.7,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                    Text(
                      "\$${price.priceEnd}",
                      textScaleFactor: 0.7,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                  ],
                )
              : Text(
                  "\$${price.standalonePrice}",
                  textScaleFactor: 0.7,
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.white),
                ),
          SizedBox(
            height: height * 0.01,
          ),
          Text(
            "${price.priceName}",
            textScaleFactor: 0.7,
            style: TextStyle(color: Colors.white),
          )
        ],
      ),
    );
  }
}
