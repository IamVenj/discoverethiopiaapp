import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FlightDetailScreen extends StatefulWidget {
  FlightDetailScreen({Key key}) : super(key: key);

  @override
  _FlightDetailScreenState createState() => _FlightDetailScreenState();
}

class _FlightDetailScreenState extends State<FlightDetailScreen> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: Colors.grey,
        body: Stack(
          children: <Widget>[
            Positioned(
                bottom: 0,
                top: height * 0.5,
                right: 0,
                left: 0,
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(width * 0.04),
                          topRight: Radius.circular(width * 0.04))),
                )),
            SvgPicture.asset(
              'images/flight/cloud.svg',
              fit: BoxFit.contain,
              height: height,
              width: width,
              alignment: Alignment.center,
            ),
            SvgPicture.asset(
              'images/flight/plane.svg',
              fit: BoxFit.contain,
              height: height,
              width: width,
              alignment: Alignment.center,
            ),
            Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding:
                        EdgeInsets.only(left: width * 0.01, top: height * 0.04),
                    child: IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: width * 0.06),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "From",
                          textScaleFactor: 0.7,
                          style: TextStyle(
                              fontSize: width * 0.07, color: Colors.white),
                        ),
                        Text(
                          "Vancouver",
                          textScaleFactor: 0.7,
                          style: TextStyle(
                              fontSize: width * 0.12,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                        Text(
                          "To",
                          textScaleFactor: 0.7,
                          style: TextStyle(
                              fontSize: width * 0.07, color: Colors.white),
                        ),
                        Text(
                          "Dubai",
                          textScaleFactor: 0.7,
                          style: TextStyle(
                              fontSize: width * 0.12,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                        Text(
                          "Via",
                          textScaleFactor: 0.7,
                          style: TextStyle(
                              fontSize: width * 0.07, color: Colors.white),
                        ),
                        Text(
                          "Toronto",
                          textScaleFactor: 0.7,
                          style: TextStyle(
                              fontSize: width * 0.12,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      left: height * 0.04,
                      right: height * 0.04,
                    ),
                    child: Container(
                      height: height * 0.5,
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Price",
                                    textScaleFactor: 0.7,
                                    style: TextStyle(
                                        fontSize: width * 0.06,
                                        color: Colors.black45),
                                  ),
                                  Text(
                                    "\$781",
                                    textScaleFactor: 0.7,
                                    style: TextStyle(
                                      fontSize: width * 0.1,
                                    ),
                                  )
                                ],
                              ),
                              Icon(
                                Icons.arrow_back_ios,
                                color: Colors.grey,
                              ),
                              FloatingActionButton(
                                  onPressed: null,
                                  backgroundColor: Colors.white,
                                  elevation: 0,
                                  heroTag: null,
                                  child: Image.asset(
                                    'images/flight/4.png',
                                    height: height * 0.055,
                                  )),
                              Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.grey,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Time",
                                    textScaleFactor: 0.7,
                                    style: TextStyle(
                                        fontSize: width * 0.06,
                                        color: Colors.black45),
                                  ),
                                  Text(
                                    "13H",
                                    textScaleFactor: 0.7,
                                    style: TextStyle(
                                      fontSize: width * 0.1,
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            height: height * 0.02,
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: width * 0.02),
                            child: Container(
                              width: width,
                              alignment: Alignment.center,
                              child: Text(
                                "Emirates",
                                textScaleFactor: 0.7,
                                style: TextStyle(
                                    fontSize: width * 0.07,
                                    fontWeight: FontWeight.w100,
                                    color: Colors.black45),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: height * 0.02,
                          ),
                        ],
                      ),
                    ),
                  ),
                ]),
            Positioned(
              bottom: 0,
              right: 0,
              child: Padding(
                padding:
                    EdgeInsets.only(left: width * 0.01, right: width * 0.01),
                child: FloatingActionButton(
                  onPressed: () {},
                  child: Icon(
                    Icons.book,
                    color: Colors.white,
                  ),
                  backgroundColor: Colors.grey,
                ),
              ),
            ),
          ],
        ));
  }
}
