import 'package:discover_ethiopia/homescreen.dart';
import 'package:discover_ethiopia/utils/utils.dart';
import 'package:flutter/material.dart';

class GetStarted extends StatefulWidget {
  GetStarted({Key key}) : super(key: key);

  @override
  _GetStartedState createState() => _GetStartedState();
}

class _GetStartedState extends State<GetStarted> {
  setGetStarted() async {
    // not to be implemented yet until design is complete
    await SharedPref().setPreference("getStarted", "true");
  }

  @override
  void initState() {
    // setGetStarted();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      height: height,
      child: Stack(
        children: <Widget>[
          Container(
            height: height,
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage(
                      'images/experience/3.jpg',
                    ))),
          ),
          Container(
            height: height,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                  Colors.black.withOpacity(0.4),
                  Colors.black.withOpacity(0.9)
                ])),
          ),
          Column(
            children: <Widget>[
              SizedBox(
                height: height * 0.67,
              ),
              Padding(
                padding:
                    EdgeInsets.only(left: width * 0.06, right: width * 0.1),
                child: Text(
                  'Get ready for your lifetime journey!',
                  textScaleFactor: 0.7,
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: "ProductSans",
                      fontWeight: FontWeight.w500,
                      decoration: TextDecoration.none,
                      fontSize: width * 0.12,
                      letterSpacing: width * 0.0009),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    left: width * 0.06, right: width * 0.1, top: height * 0.03),
                child: Text(
                  'Collection of the most beautiful places, experiences and unusual housings in the world for newbies as well as professional travelers.',
                  textScaleFactor: 0.7,
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: "ProductSans",
                      fontWeight: FontWeight.w500,
                      decoration: TextDecoration.none,
                      fontSize: width * 0.056,
                      letterSpacing: width * 0.0009),
                ),
              ),
            ],
          ),
          Positioned(
            bottom: 10,
            left: 0,
            right: 0,
            child: Container(
              padding: EdgeInsets.only(left: width * 0.02, right: width * 0.02),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(width * 0.2),
                      topRight: Radius.circular(width * 0.04))),
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => HomeScreen()));
                },
                child: Text(
                  'Get Started',
                  textScaleFactor: 0.7,
                  style: TextStyle(fontSize: width * 0.06),
                ),
                textColor: Colors.white,
                elevation: 0,
                color: Colors.black,
                padding:
                    EdgeInsets.only(top: height * 0.03, bottom: height * 0.03),
              ),
            ),
          )
        ],
      ),
    );
  }
}
